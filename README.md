```
docker run --name yourstruly_irc_sql \
	-e MYSQL_ALLOW_EMPTY_PASSWORD=yes \
	-d mysql:latest
```

```
docker run --name yourstruly_inspircd \
	--link yourstruly_irc_sql:mysql \
	-p 6660-6669:6660-6669 \
	-p 7000-7001:7000-7001 \
	-p 6697:6697 \
	-v /srv/inspircd:/inspircd/conf/ \
	-v /etc/letsencrypt/:/etc/letsencrypt \
	inspircd/inspircd-docker
```

```
docker run --name yourstruly_anope \
	--link yourstruly_inspircd:inspircd \
	--link yourstruly_irc_sql:mysql \
	-e "ANOPE_UPLINK_IP=inspircd" \
	-e "ANOPE_UPLINK_PASSWORD=polarbears" \
	-e "ANOPE_UPLINK_PORT=7000" \
	-e "ANOPE_SERVICES_VHOST=services.yourstruly.sx" \
	-e "ANOPE_SERVICES_NAME=services.yourstruly.sx" \
	-e "ANOPE_SQL_ENGINE=mysql" \
	-e "ANOPE_MYSQL_DB=yourstruly_anope" \
	-e "ANOPE_MYSQL_HOST=mysql" \
	-e "ANOPE_MYSQL_USER=root" \
	-e "ANOPE_SQL_LIVE=yes" \
	anope/anope
```


TODO: for later: 
```
docker run -d -p 80:80 -p 443:443 \
  --name nginx-proxy \
  -v /path/to/certs:/etc/nginx/certs:ro \
  -v /etc/nginx/vhost.d \
  -v /usr/share/nginx/html \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy \
  jwilder/nginx-proxy
```
